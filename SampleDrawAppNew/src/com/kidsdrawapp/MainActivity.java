package com.kidsdrawapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kidsdrawapp.SplashActivityNew.loadImage;
import com.kidsdrawapp.control.ParseDrawApiResponse;
import com.kidsdrawapp.model.DrawActivitiesModel;
import com.kidsdrawapp.utils.AndroidAppUtils;
import com.kidsdrawapp.utils.GlobalKeys;
import com.kidsdrawapp.webservices.DrawActivitiesWebservice;
import com.kidsdrawapp.webservices.GetImageAPIHandler;
import com.kidsdrawapp.webservices.VersionCheckAPIHandler;
import com.kidsdrawapp.webservices.control.WebAPIResponseListener;
import com.kidsdrawapp.webservices.control.WebserviceResponseHandler;
import com.sampledrawapp.R;

public class MainActivity extends Activity {

	// public static List<ArrayList<String>> dataList;
	// private DrawDatabase db;
	private LinearLayout myDraw;
	public static Activity mActivity;
	DownloadManager downloadManager;
	private long downloadReference;
	private String TAG = MainActivity.class.getSimpleName();
	private SmapleListAdapter ca;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mActivity = this;
		TextView headerTxt = (TextView) findViewById(R.id.header_text);
		headerTxt.setTypeface(Typeface.createFromAsset(getAssets(),
				"LDFComicSansBold.ttf"));
		RecyclerView recList = (RecyclerView) findViewById(R.id.cardList);
		recList.setHasFixedSize(true);

		GridLayoutManager llm = new GridLayoutManager(MainActivity.this, 2);
		llm.setOrientation(GridLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);

		myDraw = (LinearLayout) findViewById(R.id.myDraw);
		myDraw.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MainActivity.this, MyDrawScreen.class));
			}
		});
		ca = new SmapleListAdapter(MainActivity.this);
		recList.setAdapter(ca);
		new loadImage().execute();
		new VersionCheckAPIHandler(mActivity, webAPIResponseLinsener());

	}

	/**
	 * Download new images
	 */
	private void DownloadNewImages() {
		for (int i = 0; i < ParseDrawApiResponse.mActivitiesModels.size(); i++) {
			DrawActivitiesModel mDrawActivitiesModel = ParseDrawApiResponse.mActivitiesModels
					.get(i);
			if (!AndroidAppUtils.checkIfImageAlreadyExists(mDrawActivitiesModel
					.getImageName())) {
				new GetImageAPIHandler(mActivity,
						mDrawActivitiesModel.getImageUrl(),
						webAPIImageDowload(), mDrawActivitiesModel);
				return;
			}
		}
	}

	private WebAPIResponseListener webAPIImageDowload() {
		WebAPIResponseListener mListener = new WebAPIResponseListener() {

			@Override
			public void onSuccessOfResponse(Object... arguments) {
				// TODO Auto-generated method stub
				DownloadNewImages();
			}

			@Override
			public void onFailOfResponse(Object... arguments) {
				// TODO Auto-generated method stub

			}
		};
		return mListener;
	}

	class loadImage extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			CopyImageToSdCard();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			DownloadNewImages();
		}
	}

	/**
	 * Copy Image TO Sd Card
	 */
	private void CopyImageToSdCard() {
		for (int i = 0; i < GlobalKeys.DrawingImagesDrawableArray.length; i++) {
			copyImageFromResourceToSDCard(
					GlobalKeys.DrawingImagesDrawableName[i],
					GlobalKeys.DrawingImagesDrawableArray[i]);
		}
	}

	/**
	 * Copy Image From Drawable to storage
	 * 
	 * @param image_name
	 */
	public void copyImageFromResourceToSDCard(String image_name, int ID) {
		String targetFileName = image_name + ".png";//
		if (!AndroidAppUtils.checkIfImageAlreadyExists(image_name)) {
			if (ID != 0) {
				Bitmap bm = BitmapFactory.decodeResource(getResources(), ID);

				String PATH = Environment.getExternalStorageDirectory() + "/"
						+ GlobalKeys.STORE_IMAGE_FOLDER + "/";
				File folder = new File(PATH);
				if (!folder.exists()) {
					folder.mkdir();// If there is no folder it will be created.
				}
				File file = new File(PATH, targetFileName);
				FileOutputStream outStream;
				try {
					outStream = new FileOutputStream(file);
					bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
					outStream.flush();
					outStream.close();
					AndroidAppUtils.showLog(TAG, "Image created success");
					mActivity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							if (ca != null) {
								ca.notifyDataSetChanged();
							}
						}
					});
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			AndroidAppUtils.showLog(TAG, "This Image is already into storage");
		}

	}

	private WebAPIResponseListener webAPIResponseLinsener() {
		WebAPIResponseListener mListener = new WebAPIResponseListener() {

			@Override
			public void onSuccessOfResponse(Object... arguments) {
				// TODO Auto-generated method stub
				try {
					if (arguments.length > 0)
						onAutoUpdateAPiResponse((JSONObject) arguments[0]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailOfResponse(Object... arguments) {
				// TODO Auto-generated method stub
			}
		};
		return mListener;
	}

	/**
	 * on AutoUpdate API Response
	 *
	 * @param response
	 */
	private void onAutoUpdateAPiResponse(JSONObject response) {
		if (WebserviceResponseHandler.getInstance().checkVersionResponseCode(
				response)) {
			/* Success of API Response */
			try {
				float latest_version = Float.parseFloat(response
						.getString(GlobalKeys.LATEST_VERSION));
				final String appURI = response
						.getString(GlobalKeys.UPDATED_APP_URL);
				PackageInfo pInfo = null;
				try {
					pInfo = mActivity.getPackageManager().getPackageInfo(
							mActivity.getPackageName(), 0);
				} catch (PackageManager.NameNotFoundException e) {
					e.printStackTrace();
				}
				float versionCode = Float.parseFloat(pInfo.versionName);

				Log.d("VERSION", " VERSION FROM API : " + latest_version);
				Log.d("VERSION", " VERSION FROM APP : " + versionCode);

				if (latest_version > versionCode) {
					// oh yeah we do need an upgrade, let the user know send
					// an alert message
					AlertDialog.Builder builder = new AlertDialog.Builder(
							mActivity);
					builder.setMessage(
							"There is newer version of this application available, click OK to upgrade now?")
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										// if the user agrees to upgrade
										public void onClick(
												DialogInterface dialog, int id) {
											// start downloading the file
											// using the download manager
											downloadManager = (DownloadManager) mActivity
													.getSystemService(Context.DOWNLOAD_SERVICE);
											Uri Download_Uri = Uri
													.parse(appURI);
											DownloadManager.Request request = new DownloadManager.Request(
													Download_Uri);
											request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
											request.setAllowedOverRoaming(false);
											request.setTitle(mActivity
													.getResources().getString(
															R.string.app_name));
											request.setDestinationInExternalFilesDir(
													mActivity,
													Environment.DIRECTORY_DOWNLOADS,
													GlobalKeys.APK_NAME
															+ ".apk");
											downloadReference = downloadManager
													.enqueue(request);
										}
									});
					// show the alert message
					builder.create().show();
				} else {
					// mResponseListener.onSuccessOfResponse();
				}

			} catch (Exception e) {
				e.printStackTrace();
				// mResponseListener.onSuccessOfResponse();
			}

		} else {
			/* Fail of API Response */
			// AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
			// "Something Went wrong !!", null);
			// mResponseListener.onSuccessOfResponse();
		}
	}

	// broadcast receiver to get notification about ongoing downloads
	private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			// check if the broadcast message is for our Enqueued download
			long referenceId = intent.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			if (downloadReference == referenceId) {

				AndroidAppUtils.showVerboseLog("VersionCheckAPIHandler",
						"Downloading of the new app version complete");
				// start the installation of the latest version
				Intent installIntent = new Intent(Intent.ACTION_VIEW);
				installIntent.setDataAndType(downloadManager
						.getUriForDownloadedFile(downloadReference),
						"application/vnd.android.package-archive");
				installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mActivity.startActivity(installIntent);

			}
		}
	};

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		IntentFilter filter = new IntentFilter(
				DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		mActivity.registerReceiver(downloadReceiver, filter);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if (downloadReceiver != null) {
				mActivity.unregisterReceiver(downloadReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
