package com.kidsdrawapp;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sampledrawapp.R;

/**
 * Created by Harish on 12/28/2015.
 */
public class MyDrawListAdapter extends BaseAdapter {

	ArrayList<File> mDrawList = new ArrayList<>();

	/**
	 * Context object
	 */
	private Context context;
	@SuppressWarnings("unused")
	private LayoutInflater mLayoutInflater;

	public MyDrawListAdapter(Activity activity) {
		this.context = activity;
		try {
			mLayoutInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * * Add updated data on List
	 */
	public void addUpdateDataIntoList(ArrayList<File> StringArrayList) {
		this.mDrawList = StringArrayList;
	}

	@Override
	public int getCount() {
		if (mDrawList != null) {
			return mDrawList.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return mDrawList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(context.getApplicationContext(),
					R.layout.my_draw_row, null);
			new ViewHolder(convertView);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (mDrawList != null) {
			Log.d("DEBUG",
					"mDrawList.get(position) :" + mDrawList.get(position));
			Bitmap myBitmap = BitmapFactory.decodeFile(mDrawList.get(position)
					.getAbsolutePath());
			holder.myDrawImage.setImageBitmap(myBitmap);

			holder.deleteIcon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mDrawList.get(position).delete();
					mDrawList.remove(position);
					notifyDataSetChanged();
					if (mDrawList != null && mDrawList.isEmpty()) {
						if (MyDrawScreen.noActTxt != null) {
							MyDrawScreen.noActTxt.setVisibility(View.VISIBLE);
						}
					}
				}
			});
		} else {
			Log.e("DENUG", "mDrawList is null");
		}
		return convertView;
	}

	private class ViewHolder {
		ImageView myDrawImage;
		RelativeLayout deleteIcon;

		public ViewHolder(View view) {
			myDrawImage = (ImageView) view.findViewById(R.id.myDrawImage);
			deleteIcon = (RelativeLayout) view.findViewById(R.id.deleteIcon);
			view.setTag(this);
		}
	}
}
