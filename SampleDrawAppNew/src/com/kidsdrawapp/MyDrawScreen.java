package com.kidsdrawapp;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kidsdrawapp.utils.AndroidAppUtils;
import com.kidsdrawapp.utils.GlobalKeys;
import com.sampledrawapp.R;

/**
 * Created by Shruti on 1/22/2016.
 */
public class MyDrawScreen extends Activity {

	private MyDrawListAdapter mMyDrawListAdapter;
	private ListView drawList;
	public static TextView noActTxt;
	private ImageView hedaerBack;
	private Activity mActivity;
	private ArrayList<File> mDrawList;
	private String TAG = MyDrawScreen.class.getSimpleName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_draw_screen);
		initViews();
		new GetFileFromStorage().execute();
	}

	/**
	 * Task is process of background to get video files.
	 */
	class GetFileFromStorage extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			getListOfVideosData();
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (mDrawList != null && !mDrawList.isEmpty()) {
				mMyDrawListAdapter.addUpdateDataIntoList(mDrawList);
				mMyDrawListAdapter.notifyDataSetChanged();
				noActTxt.setVisibility(View.GONE);
			} else {
				noActTxt.setVisibility(View.VISIBLE);
			}

		}
	}

	/**
	 * Gets List of app Video files
	 */
	private void getListOfVideosData() {
		try {
			mDrawList = new ArrayList<>();
			String path = Environment.getExternalStorageDirectory().toString()
					+ "/" + GlobalKeys.MY_DRAW_FOLDER;
			File f = new File(path);
			File file[] = f.listFiles();
			// Arrays.sort(file,
			// LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);
			// Arrays.sort(file,
			// LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			AndroidAppUtils.showLog(TAG, "Size: " + file.length);
			if (file.length > 0)
				noActTxt.setVisibility(View.GONE);
			else
				noActTxt.setVisibility(View.VISIBLE);
			for (int i = 0; i < file.length; i++) {
				String fileName = file[i].getName();
				AndroidAppUtils.showLog(TAG, "FileName :" + fileName);
				if (fileName.contains(".jpg")) {
					fileName = fileName.substring(0, fileName.lastIndexOf('.'));
					mDrawList.add(file[i]);
				} else
					AndroidAppUtils.showErrorLog(TAG,
							"This is not Recored Video");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*******************************************************************************
	 * Function name - initViews Description - initialize views included in the
	 * layout
	 ******************************************************************************/
	private void initViews() {
		mActivity = MyDrawScreen.this;
		mMyDrawListAdapter = new MyDrawListAdapter(mActivity);
		drawList = (ListView) mActivity.findViewById(R.id.myDrawList);
		noActTxt = (TextView) mActivity.findViewById(R.id.noActTxt);
		hedaerBack = (ImageView) mActivity.findViewById(R.id.header_icon);
		noActTxt.setVisibility(View.GONE);
		drawList.setAdapter(mMyDrawListAdapter);
		hedaerBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}

}
