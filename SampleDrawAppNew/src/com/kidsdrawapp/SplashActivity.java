//package com.kidsdrawapp;
//
//import java.io.BufferedInputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.lang.reflect.Field;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.res.AssetManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.Handler;
//import android.util.Log;
//import android.view.View;
//import android.widget.ProgressBar;
//
//import com.kidsdrawapp.utils.AndroidAppUtils;
//import com.kidsdrawapp.web.API;
//import com.kidsdrawapp.web.RestClient;
//import com.sampledrawapp.R;
//
//public class SplashActivity extends Activity implements API {
//
//	public static final String downloadFolder = "DrawApp";
//	ProgressBar progressBar;
//	DrawDatabase db;
//	HashMap<String, Long> downloadState;
//	HashMap<String, ArrayList<String>> downloadData;
//	Activity mActivity;
//	String TAG = SplashActivity.class.getSimpleName();
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.splash);
//		mActivity = this;
//		initProcess();
//	}
//
//	private void initProcess() {
//
//		db = new DrawDatabase(SplashActivity.this);
//
//		if (db.getAllCotacts().size() <= 0) {
//			// copy asset data to SDcard
//			copyAssetToSDcard();
//			// add two row to DB
//			// db.insertContact
//			// ("1", getText(R.string.title_1).toString(),
//			// getText(R.string.story_1).toString(), "thumb_1",
//			// "image_1");
//
//			// db.insertContact
//			// ("2", getText(R.string.title_2).toString(),
//			// getText(R.string.story_2).toString(), "thumb_2",
//			// "image_2");
//		}
//
//		progressBar = (ProgressBar) findViewById(R.id.progressbar);
//		if (haveNetworkConnection()) {
//			hitUrl();
//		} else {
//			// Intent intent = new Intent(SplashActivity.this,
//			// MainActivity.class);
//			// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			// startActivity(intent);
//			new Handler().postDelayed(new Runnable() {
//
//				@Override
//				public void run() {
//					String response = LoadData("responseJson.txt");
//					parseTheResponse(response);
//				}
//			}, 2000);
//
//		}
//	}
//
//	private void hitUrl() {
//
//		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//
//			@Override
//			protected void onPreExecute() {
//				super.onPreExecute();
//				progressBar.setVisibility(View.VISIBLE);
//			}
//
//			@Override
//			protected void onPostExecute(Void result) {
//				super.onPostExecute(result);
//				// progressBar.setVisibility(View.INVISIBLE);
//				if (downloadState != null && downloadState.size() == 0) {
//					progressBar.setVisibility(View.INVISIBLE);
//					// Move to DashBoard
//					movToNextScreen();
//				}
//			}
//
//			@Override
//			protected Void doInBackground(Void... arg0) {
//
//				RestClient restClient = new RestClient(BASE_URL);
//				String response = restClient.executeRequest();
//				parseTheResponse(response);
//				return null;
//			}
//
//		};
//
//		task.execute();
//	}
//
//	public void parseTheResponse(String response) {
//		String replyCode = "success";
//		try {
//			if (response != null && !response.equalsIgnoreCase("error")) {
//				JSONObject jObject = new JSONObject(response);
//				replyCode = jObject.getString("replyCode");
//				if (replyCode.equalsIgnoreCase("success")) {
//					// proceed to DB Work
//
//					downloadState = new HashMap<String, Long>();
//					downloadData = new HashMap<String, ArrayList<String>>();
//
//					JSONArray array = jObject.getJSONArray("data");
//					Log.d("SplashActivity ", "jObject : " + jObject);
//					for (int i = 0; i < array.length(); i++) {
//						final JSONObject obj = array.getJSONObject(i);
//						runOnUiThread(new Runnable() {
//							@Override
//							public void run() {
//								try {
//									String id = obj.getString("id");
//
//									if (db.getData(obj.getString("id"))
//											.getCount() <= 0) {
//
//										String thumb_name = "thumb_" + id;
//										String image_name = "image_" + id;
//
//										downloadState.put(thumb_name, 0L);
//										downloadState.put(image_name, 0L);
//
//										ArrayList<String> data = new ArrayList<String>();
//										data.add(obj.getString("id"));
//										data.add(obj.getString("title"));
//										data.add(obj.getString("description"));
//										data.add(thumb_name);
//										data.add(image_name);
//
//										downloadData.put(thumb_name, data);
//										if (haveNetworkConnection()) {
//											AndroidAppUtils.showLog(TAG,
//													"have connection");
//											if (getId(image_name,
//													R.drawable.class) != 0) {
//												onPostWork(null, image_name);
//											} else {
//												new DownloadFile(image_name)
//														.executeOnExecutor(
//																AsyncTask.SERIAL_EXECUTOR,
//																obj.getString("image"));
//											}
//											if (getId(thumb_name,
//													R.drawable.class) != 0) {
//												onPostWork(null, thumb_name);
//											} else {
//												new DownloadFile(thumb_name)
//														.executeOnExecutor(
//																AsyncTask.SERIAL_EXECUTOR,
//																obj.getString("thumb"));
//											}
//
//										} else {
//											AndroidAppUtils.showLog(TAG,
//													"have no connection");
//											onPostWork(null, thumb_name);
//											onPostWork(null, image_name);
//										}
//
//									}
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
//							}
//						});
//					}
//					if (!haveNetworkConnection()) {
//						// Move to inside page
//						AndroidAppUtils.showLog(TAG, "here ");
//						movToNextScreen();
//					}
//				} else {
//					AndroidAppUtils.showLog(TAG, "or  here ");
//					// Move to inside page
//					movToNextScreen();
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	class DownloadFile extends AsyncTask<String, Integer, Long> {
//		ProgressDialog mProgressDialog = new ProgressDialog(SplashActivity.this);// Change
//																					// Mainactivity.this
//																					// with
//																					// your
//																					// activity
//																					// name.
//		String strFolderName;
//		String name = "";
//
//		public DownloadFile(String name) {
//			this.name = name;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			progressBar.setVisibility(View.VISIBLE);
//		}
//
//		@Override
//		protected Long doInBackground(String... aurl) {
//			int count;
//			try {
//				URL url = new URL((String) aurl[0]);
//				URLConnection conexion = url.openConnection();
//				conexion.connect();
//				String targetFileName = name + ".png";// Change name and
//														// subname
//				int lenghtOfFile = conexion.getContentLength();
//				String PATH = Environment.getExternalStorageDirectory() + "/"
//						+ downloadFolder + "/";
//				File folder = new File(PATH);
//				if (!folder.exists()) {
//					folder.mkdir();// If there is no folder it will be created.
//				}
//				InputStream input = new BufferedInputStream(url.openStream());
//				OutputStream output = new FileOutputStream(PATH
//						+ targetFileName);
//				byte data[] = new byte[1024];
//				long total = 0;
//				while ((count = input.read(data)) != -1) {
//					total += count;
//					publishProgress((int) (total * 100 / lenghtOfFile));
//					output.write(data, 0, count);
//					downloadState.put(this.name, total);
//				}
//				output.flush();
//				output.close();
//				input.close();
//				return total;
//			} catch (Exception e) {
//			}
//			return null;
//		}
//
//		protected void onProgressUpdate(Integer... progress) {
//			Log.d("Percentage ... " + this.name, "" + progress[0]);
//		}
//
//		protected void onPostExecute(Long result) {
//			Log.d("result ... ", "Complete .. " + this.name + "-" + result);
//			onPostWork(result, name);
//
//		}
//	}
//
//	private void onPostWork(Long result, String name) {
//		AndroidAppUtils.showLog(TAG, "onPostWork :  " + name);
//		if (result == null) {
//			if (!AndroidAppUtils.checkIfImageAlreadyExists(name)) {
//				copyImageFromResourceToSDCard(name);
//			}
//			result = (long) 1234;
//		}
//
//		if (result != null && name.contains("thumb")) {
//			ArrayList<String> data = downloadData.get(name);
//			boolean b = db.insertContact(data.get(0), data.get(1), data.get(2),
//					data.get(3), data.get(4));
//			Log.d("Row Inserted", "" + b);
//		}
//
//		downloadState.remove(name);
//
//		// if (downloadState.size() == 0) {
//		// progressBar.setVisibility(View.INVISIBLE);
//		// // Move to DashBoard
//		// movToNextScreen();
//		// }
//	}
//
//	private boolean haveNetworkConnection() {
//		boolean haveConnectedWifi = false;
//		boolean haveConnectedMobile = false;
//
//		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//		for (NetworkInfo ni : netInfo) {
//			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//				if (ni.isConnected())
//					haveConnectedWifi = true;
//			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//				if (ni.isConnected())
//					haveConnectedMobile = true;
//		}
//		return haveConnectedWifi || haveConnectedMobile;
//	}
//
//	public void copyImageFromResourceToSDCard(String image_name) {
//		Log.d("SplashActivity", "copyImageFromResourceToSDCard : " + image_name);
//		String targetFileName = image_name + ".png";//
//
//		int res_id = getId(image_name, R.drawable.class);
//		if (res_id != 0) {
//			Bitmap bm = BitmapFactory.decodeResource(getResources(), res_id);
//			String PATH = Environment.getExternalStorageDirectory() + "/"
//					+ downloadFolder + "/";
//			File folder = new File(PATH);
//			if (!folder.exists()) {
//				folder.mkdir();// If there is no folder it will be created.
//			}
//			File file = new File(PATH, targetFileName);
//			FileOutputStream outStream;
//			try {
//				outStream = new FileOutputStream(file);
//				bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
//				outStream.flush();
//				outStream.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//	}
//
//	private void copyAssetToSDcard() {
//		AssetManager assetManger = getAssets();
//		String files[] = null;
//		try {
//			files = assetManger.list("");
//		} catch (Exception e) {
//			Log.e("ERROR", "Failed to get files from asset");
//		}
//
//		if (files != null) {
//			for (String fileName : files) {
//				InputStream in = null;
//				OutputStream out = null;
//
//				try {
//					in = assetManger.open(fileName);
//					File outFile = new File(
//							Environment.getExternalStorageDirectory() + "/"
//									+ downloadFolder + "/", fileName);
//					String p = outFile.getAbsolutePath();
//					int i = p.lastIndexOf(".");
//					if (i > 0) {
//						if (p.substring(i + 1).equalsIgnoreCase("png")) {
//							out = new FileOutputStream(outFile);
//							copyFile(in, out);
//						}
//					}
//				} catch (Exception e) {
//					Log.e("ERROR", "Failed to copy asset file");
//				} finally {
//					if (in != null) {
//						try {
//							in.close();
//						} catch (Exception e) {
//
//						}
//					}
//
//					if (out != null) {
//						try {
//							out.close();
//						} catch (Exception e) {
//
//						}
//					}
//				}
//			}
//		}
//	}
//
//	private void copyFile(InputStream in, OutputStream out) throws IOException {
//		byte[] buffer = new byte[1024];
//		int read;
//		while ((read = in.read(buffer)) != -1) {
//			out.write(buffer, 0, read);
//		}
//	}
//
//	/**
//	 * to get id of relative layout by passing string
//	 * 
//	 * @param resourceName
//	 * @param c
//	 * @return
//	 */
//	public static int getId(String resourceName, Class<?> c) {
//		try {
//			Field idField = c.getDeclaredField(resourceName);
//			return idField.getInt(idField);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	public String LoadData(String inFile) {
//		String tContents = "";
//
//		try {
//			InputStream stream = getAssets().open(inFile);
//
//			int size = stream.available();
//			byte[] buffer = new byte[size];
//			stream.read(buffer);
//			stream.close();
//			tContents = new String(buffer);
//		} catch (IOException e) {
//			// Handle exceptions here
//		}
//
//		return tContents;
//
//	}
//
//	private void movToNextScreen() {
//		Log.d("DEBU", "MOVE TO NEXT");
//		if (MainActivity.mActivity != null)
//			MainActivity.mActivity.finish();
//		Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//		startActivity(intent);
//		finish();
//
//	}
// }
