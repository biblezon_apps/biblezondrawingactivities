package com.kidsdrawapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import com.kidsdrawapp.control.ParseDrawApiResponse;
import com.kidsdrawapp.utils.AndroidAppUtils;
import com.kidsdrawapp.utils.GlobalKeys;
import com.kidsdrawapp.webservices.DrawActivitiesWebservice;
import com.kidsdrawapp.webservices.control.WebAPIResponseListener;
import com.sampledrawapp.R;

public class SplashActivityNew extends Activity {
	/**
	 * Activity Instance
	 */
	private Activity mActivity;
	/**
	 * Debugging Tag
	 */
	private String TAG = SplashActivityNew.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		initViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		new loadImage().execute();
	}

	class loadImage extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			new ParseDrawApiResponse(mActivity, null,
					LoadData("responseJson.txt"));
			CopyImageToSdCard();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			new DrawActivitiesWebservice(mActivity, onAPIResponse());
		}

	}

	/**
	 * init views
	 */
	private void initViews() {
		mActivity = this;
	}

	/**
	 * On API Response
	 * 
	 * @return
	 */
	private WebAPIResponseListener onAPIResponse() {
		WebAPIResponseListener mListener = new WebAPIResponseListener() {

			@Override
			public void onSuccessOfResponse(Object... arguments) {
				moveToNextScreen();
			}

			@Override
			public void onFailOfResponse(Object... arguments) {

			}
		};
		return mListener;
	}

	/**
	 * Copy Image TO Sd Card
	 */
	private void CopyImageToSdCard() {
		for (int i = 0; i < 4; i++) {
			copyImageFromResourceToSDCard(
					GlobalKeys.DrawingImagesDrawableName[i],
					GlobalKeys.DrawingImagesDrawableArray[i]);
		}
	}

	/**
	 * Copy Image From Drawable to storage
	 * 
	 * @param image_name
	 */
	public void copyImageFromResourceToSDCard(String image_name, int ID) {
		String targetFileName = image_name + ".png";//
		if (!AndroidAppUtils.checkIfImageAlreadyExists(image_name)) {
			if (ID != 0) {
				Bitmap bm = BitmapFactory.decodeResource(getResources(), ID);

				String PATH = Environment.getExternalStorageDirectory() + "/"
						+ GlobalKeys.STORE_IMAGE_FOLDER + "/";
				File folder = new File(PATH);
				if (!folder.exists()) {
					folder.mkdir();// If there is no folder it will be created.
				}
				File file = new File(PATH, targetFileName);
				FileOutputStream outStream;
				try {
					outStream = new FileOutputStream(file);
					bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
					outStream.flush();
					outStream.close();
					AndroidAppUtils.showLog(TAG, "Image created success");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			AndroidAppUtils.showLog(TAG, "This Image is already into storage");
		}

	}

	/**
	 * Load Assest Data
	 * 
	 * @param inFile
	 * @return
	 */
	public String LoadData(String inFile) {
		String tContents = "";
		try {
			InputStream stream = mActivity.getAssets().open(inFile);

			int size = stream.available();
			byte[] buffer = new byte[size];
			stream.read(buffer);
			stream.close();
			tContents = new String(buffer);
		} catch (IOException e) {
			// Handle exceptions here
		}

		return tContents;

	}

	/**
	 * Move to next Screen
	 */
	private void moveToNextScreen() {
		if (MainActivity.mActivity != null)
			MainActivity.mActivity.finish();
		Intent intent = new Intent(SplashActivityNew.this, MainActivity.class);
		startActivity(intent);
		finish();

	}
}
