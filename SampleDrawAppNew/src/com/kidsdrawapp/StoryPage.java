package com.kidsdrawapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.kidsdrawapp.control.ParseDrawApiResponse;
import com.sampledrawapp.R;

public class StoryPage extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.story_page);

		ImageView backBtn = (ImageView) findViewById(R.id.header_icon);
		backBtn.setImageResource(R.drawable.left_arrow);
		backBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		int key = getIntent().getExtras().getInt("key", 0);
		getStory(key);
	}

	private void getStory(int key) {
		TextView headerText = (TextView) findViewById(R.id.header_text);
		TextView storyText = (TextView) findViewById(R.id.story_content);

		headerText.setText(ParseDrawApiResponse.mActivitiesModels.get(key)
				.getImageTitle());
		storyText.setText(ParseDrawApiResponse.mActivitiesModels.get(key)
				.getImageDescription());

		headerText.setTypeface(Typeface.createFromAsset(getAssets(),
				"LDFComicSansBold.ttf"));
		storyText.setTypeface(Typeface.createFromAsset(getAssets(),
				"LDFComicSansLight.ttf"));
	}

}
