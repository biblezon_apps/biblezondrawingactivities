//package com.kidsdrawapp.db;
//
//import java.util.ArrayList;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//
//public class DrawDatabase extends SQLiteOpenHelper {
//
//	public static final String DATABASE_NAME = "KidsDrawApp.db";
//	public static final int DATABASE_VERSION = 2;
//	public static final String CONTACTS_TABLE_NAME = "KidsDraw";
//
//	public static final String COLUMN_ID = "id";
//	public static final String COLUMN_KEY = "key";
//	public static final String COLUMN_NAME = "name";
//	public static final String COLUMN_DESCRIPTION = "description";
//	public static final String COLUMN_IMAGE_THUMB = "image_path_thumb";
//	public static final String COLUMN_IMAGE_MAIN = "image_path_main";
//
//	public DrawDatabase(Context context) {
//		super(context, DATABASE_NAME, null, DATABASE_VERSION);
//	}
//
//	@Override
//	public void onCreate(SQLiteDatabase db) {
//		db.execSQL("create table KidsDraw "
//				+ "(id integer primary key,key text,name text,description text,image_path_thumb text, image_path_main text)");
//	}
//
//	@Override
//	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
//		db.execSQL("DROP TABLE IF EXISTS KidsDraw");
//		onCreate(db);
//	}
//
//	public boolean insertContact(String key, String name, String description,
//			String image_thumb, String image_main) {
//		SQLiteDatabase db = this.getWritableDatabase();
//
//		Cursor data = getData(key);
//
//		if (data.getCount() <= 0) {
//			ContentValues contentValues = new ContentValues();
//			contentValues.put(COLUMN_KEY, key);
//			contentValues.put(COLUMN_NAME, name);
//			contentValues.put(COLUMN_DESCRIPTION, description);
//			contentValues.put(COLUMN_IMAGE_THUMB, image_thumb);
//			contentValues.put(COLUMN_IMAGE_MAIN, image_main);
//			db.insert(CONTACTS_TABLE_NAME, null, contentValues);
//			return true;
//		}
//		return false;
//	}
//
//	public Cursor getData(String id) {
//		SQLiteDatabase db = this.getReadableDatabase();
//		Cursor res = db.rawQuery("select * from " + CONTACTS_TABLE_NAME
//				+ " where key=" + id + "", null);
//		return res;
//	}
//
//	public ArrayList<ArrayList<String>> getAllCotacts() {
//		ArrayList<ArrayList<String>> array_list = new ArrayList<ArrayList<String>>();
//
//		// hp = new HashMap();
//		SQLiteDatabase db = this.getReadableDatabase();
//		Cursor res = db.rawQuery("select * from " + CONTACTS_TABLE_NAME, null);
//		res.moveToFirst();
//
//		while (res.isAfterLast() == false) {
//			ArrayList<String> data = new ArrayList<String>();
//			data.add(res.getString(res.getColumnIndex(COLUMN_KEY)));
//			data.add(res.getString(res.getColumnIndex(COLUMN_NAME)));
//			data.add(res.getString(res.getColumnIndex(COLUMN_DESCRIPTION)));
//			data.add(res.getString(res.getColumnIndex(COLUMN_IMAGE_THUMB)));
//			data.add(res.getString(res.getColumnIndex(COLUMN_IMAGE_MAIN)));
//			array_list.add(data);
//			res.moveToNext();
//		}
//		return array_list;
//	}
// }
