package com.kidsdrawapp.utils;

/**
 * App Helper of the application
 * 
 * @author Anshuman
 * 
 */
public interface AppHelper {

	int ONE_SECOND = 1000;
	int API_REQUEST_TIME = 20;
	boolean APP_UNIT_TESTING_ON = true;
	int IMAGE_DEFAULT_SIZE = 800;
	
}
