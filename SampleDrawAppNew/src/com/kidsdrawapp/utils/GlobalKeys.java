package com.kidsdrawapp.utils;

import com.sampledrawapp.R;

/**
 * Application Keys
 * 
 * @author Anshuman
 * 
 */
public interface GlobalKeys {

	/**
	 * version check keys
	 */
	String VERSION_CHECK_BASEURL = "http://biblezon.com/appapicms/webservices/autoupdate/";
	String VERSION_CHECK_KEY = "version_check";
	String MSG_SUCCESS = "success";
	String LATEST_VERSION = "latestVersion";
	String UPDATED_APP_URL = "appURI";
	String APK_NAME = "Activity";

	String AUTO_UPDATE_URL = "http://biblezonadmin.com/biblezon/apk/checkversion.php?app=com.sampledrawapp&deviceId=";

	String MY_DRAW_FOLDER = "MyDrawImages";
	String STORE_IMAGE_FOLDER = "DrawApp";
	// Increase it for big size and decrease it for make color box in small
	// size.
	int ColorBox_Size = 60;

	/**
	 * 
	 * Drawing Images
	 */
	public static String[] DrawingImagesDrawableName = { "image_16",
			"image_17", "image_18", "image_19", "image_20", "image_21",
			"image_22", "image_23", "image_24", "image_25", "image_28",
			"image_30", "image_31", "image_32", "image_33", "image_34",
			"image_35", "image_36", "image_37", "image_38", "image_39",
			"image_40", "image_41", "image_42", "image_43",

	/*
	 * "thumb_16", "thumb_17", "thumb_18", "thumb_19", "thumb_20", "thumb_21",
	 * "thumb_22", "thumb_23", "thumb_24", "thumb_25", "thumb_28", "thumb_30",
	 * "thumb_31", "thumb_32", "thumb_33", "thumb_34", "thumb_35", "thumb_36",
	 * "thumb_37", "thumb_38", "thumb_39", "thumb_40", "thumb_41", "thumb_42",
	 * "thumb_43"
	 */};

	/**
	 * 
	 * Drawing Images
	 */
	public static int[] DrawingImagesDrawableArray = { R.drawable.image_16,
			R.drawable.image_17, R.drawable.image_18, R.drawable.image_19,
			R.drawable.image_20, R.drawable.image_21, R.drawable.image_22,
			R.drawable.image_23, R.drawable.image_24, R.drawable.image_25,
			R.drawable.image_28, R.drawable.image_30, R.drawable.image_31,
			R.drawable.image_32, R.drawable.image_33, R.drawable.image_34,
			R.drawable.image_35, R.drawable.image_36, R.drawable.image_37,
			R.drawable.image_38, R.drawable.image_39, R.drawable.image_40,
			R.drawable.image_41, R.drawable.image_42, R.drawable.image_43,

	/*
	 * R.drawable.thumb_16, R.drawable.thumb_17, R.drawable.thumb_18,
	 * R.drawable.thumb_19, R.drawable.thumb_20, R.drawable.thumb_21,
	 * R.drawable.thumb_22, R.drawable.thumb_23, R.drawable.thumb_24,
	 * R.drawable.thumb_25, R.drawable.thumb_28, R.drawable.thumb_30,
	 * R.drawable.thumb_31, R.drawable.thumb_32, R.drawable.thumb_33,
	 * R.drawable.thumb_34, R.drawable.thumb_35, R.drawable.thumb_36,
	 * R.drawable.thumb_37, R.drawable.thumb_38, R.drawable.thumb_39,
	 * R.drawable.thumb_40, R.drawable.thumb_41, R.drawable.thumb_42,
	 * R.drawable.thumb_43
	 */};

}
