package com.kidsdrawapp.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class RestClient {
	
	OkHttpClient client;
	String url;
	
	public RestClient(String url){
		this.url = url;
		client = new OkHttpClient();
	}
	
	public String executeRequest(){
		
		String response = "ERROR";
		
		try{
			 Request request = new Request.Builder().url(url).build();	
			 Response res = client.newCall(request).execute();
			 response = convertStreamToString(res.body().byteStream()); 
			 return response;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return response;
	}
	
	private static String convertStreamToString(InputStream is) {

	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append((line + "\n"));
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
	}
	
	
	
}
