package com.kidsdrawapp.webservices;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kidsdrawapp.application.ActivityApplicationController;
import com.kidsdrawapp.utils.AndroidAppUtils;
import com.kidsdrawapp.utils.GlobalKeys;
import com.kidsdrawapp.webservices.control.WebAPIResponseListener;
import com.kidsdrawapp.webservices.control.WebserviceAPIErrorHandler;

/**
 * check version of application
 * 
 * @author Shruti
 * 
 */
public class VersionCheckAPIHandler {
	private Activity mActivity;
	@SuppressWarnings("unused")
	private Context context;
	/**
	 * Debug TAG
	 */
	private String TAG = VersionCheckAPIHandler.class.getSimpleName();
	/**
	 * API Response Listener
	 */
	private WebAPIResponseListener mResponseListener;

	/**
	 * 
	 * @param mActivity
	 * @param webAPIResponseListener
	 * @param MassUrl
	 */
	public VersionCheckAPIHandler(Activity mActivity,
			WebAPIResponseListener webAPIResponseListener) {
		this.mActivity = mActivity;
		this.context = mActivity;
		this.mResponseListener = webAPIResponseListener;
		postAPICall();

	}

	/**
	 * Making json object request
	 * 
	 * @param massUrl
	 * 
	 */
	public void postAPICall() {
		/**
		 * JSON Request
		 */
		String version_url = (GlobalKeys.AUTO_UPDATE_URL + Settings.Secure
				.getString(mActivity.getContentResolver(),
						Settings.Secure.ANDROID_ID)).trim();
		AndroidAppUtils.showLog(TAG, "version_url : " + version_url);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
				version_url, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						AndroidAppUtils.showInfoLog(TAG, "Response :"
								+ response);
						mResponseListener.onSuccessOfResponse(response);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						WebserviceAPIErrorHandler.getInstance()
								.VolleyErrorHandler(error, mActivity);
						AndroidAppUtils.hideProgressDialog();
						if (mResponseListener != null)
							mResponseListener.onSuccessOfResponse();
					}
				}) {

		};

		// Adding request to request queue
		ActivityApplicationController.getInstance().addToRequestQueue(
				jsonObjReq, GlobalKeys.VERSION_CHECK_KEY);
		// set request time-out
		jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(1000 * 20,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Canceling request
		// MassAppApplicationController.getInstance().getRequestQueue()
		// .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
	}
}
